//
//  CheckActionTableViewCell.swift
//  CheckAction
//
//  Created by Denis Lyakhovich on 21.03.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit
import CoreData

class CheckActionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var backView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        actionLabel.layer.cornerRadius = actionLabel.frame.height / 2.5
        actionLabel.layer.masksToBounds = true
    }
 
    func viewCell (action: NSManagedObject) {
        actionLabel.text = action.value(forKeyPath: "action") as? String
        self.actionLabel.resignFirstResponder()
    }
}
