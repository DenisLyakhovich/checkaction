//
//  CheckActionViewController.swift
//  CheckAction
//
//  Created by Denis Lyakhovich on 21.03.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import UIKit
import CoreData

class CheckActionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var actionCheck: [NSManagedObject] = []
   
    @IBAction func addAction(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "ACTION",message: "Add a new action",preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save",style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let actionToSave = textField.text else {
                    return
            }
            
            self.save(action: actionToSave)
            self.tableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName:"CheckActionTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckActionTableViewCell")
        self.editButtonItem.tintColor = UIColor.white
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let managedContext = CoreDataManager.instance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Check")
        do {
            actionCheck = try managedContext.fetch(fetchRequest)
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        tableView.reloadData()
    }
    
    // MARK: - TableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actionCheck.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckActionTableViewCell") as! CheckActionTableViewCell
        cell.viewCell(action: actionCheck[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
   
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Check") {
            _, indexPath in
            let commit = self.actionCheck[indexPath.row]
            CoreDataManager.instance.persistentContainer.viewContext.delete(commit)
            self.actionCheck.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            CoreDataManager.instance.saveContext()
            
        }
        deleteAction.backgroundColor = UIColor.black
        return [deleteAction]
    }

    func save(action: String) {
        let managedContext = CoreDataManager.instance.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Check", in: managedContext)!
        let check = NSManagedObject(entity: entity, insertInto: managedContext)
        check.setValue(action, forKeyPath: "action")
        do {
            try managedContext.save()
            actionCheck.append(check)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}
    


