//
//  Check+CoreDataProperties.swift
//  CheckAction
//
//  Created by Denis Lyakhovich on 23.03.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//
//

import Foundation
import CoreData


extension Check {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Check> {
        return NSFetchRequest<Check>(entityName: "Check")
    }

    @NSManaged public var action: String?
}
