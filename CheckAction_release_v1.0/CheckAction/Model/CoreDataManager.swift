//
//  CoreDataManager.swift
//  CheckAction
//
//  Created by Denis Lyakhovich on 22.03.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

import CoreData
import Foundation

class CoreDataManager: NSObject {
    
    // Singleton
    static let instance = CoreDataManager()
    
    override private init() {}
    
    lazy var persistentContainer: NSPersistentContainer = {
       
        let container = NSPersistentContainer(name: "ModelData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
